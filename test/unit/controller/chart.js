describe('ChartCtrl', function () {

  var scope, ctrl, httpBackend;

  beforeEach(function () {
    module('lastfm');
    inject(function ($rootScope, $controller, $httpBackend) {
      scope = $rootScope.$new();

      httpBackend = $httpBackend;

      httpBackend
        .whenGET('http://localhost:3000/')
        .respond(200, {});

      ctrl = $controller('ChartCtrl', {$scope: scope});

      httpBackend.flush();
    });
  });

  it('should set an inital chart', function () {
    // expect(scope.chartData).to.be.an('object').with.keys(['width','height','options','data']);
  });

  it('should collect information from database', function () {
    expect(scope.songs).to.be.an('object');
  });

  describe('#changeArtist', function() {
    it('should be a function', function () {
      expect(scope.changeArtist).to.be.a('function')
    });
  });

});
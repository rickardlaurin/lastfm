angular.module('lastfm').controller('ChartCtrl', function ($scope, $http, api, $rootScope) {
  
  'use strict';

  var options = {
    scaleStepWidth : 1,
    scaleShowGridLines: false,
    datasetFill: false,
    scaleLineColor : "rgba(0,0,0,0)",
    scaleFontColor : "#aaa",  
    datasetStrokeWidth : 2,
    pointDotRadius : 2,
    pointDotStrokeWidth : 0,
  };

  api.get('http://localhost:4000/')
    .then(function (songs) {
      $scope.songs = songs;
    });

  $scope.$watch('songs', function (years) {
    if (!years) { return; }

    var lengths = [];
    var months = [];
    var artist;

    Object.keys(years).map(function (year) {
      Object.keys(years[year]).map(function (month, i) {
        if (years[year][month]) {
          lengths.push(years[year][month].length);
          artist = years[year][month][0].artist["#text"];
        } else {
          lengths.push(0);
        }

        if (i % 6 === 0) {
          months.push(moment(year + ' ' + (parseInt(month,10)+1)).month(month).format('MMMM') + ' ' + year);
        } else {
          months.push("");
        }
      });
    });

    $scope.artist = artist;

    var data = {
      labels: months,
      datasets: [
        {
          strokeColor: '#F35C9F',
          data: lengths
        }
      ]
    };

    $scope.myChart = { data:data, options:options };
  });

  $scope.changeArtist = function () {
    api.get('http://localhost:3000/?q=' + $scope.artist)
      .then(function (songs) {
        $scope.songs = songs;
      });
  };

  $scope.myChart = {
    options : options,
    data : {
      labels : ["January","February","March","April","May","June","July"],
      datasets: [
        {
          strokeColor: '#F35C9F',
          data: [0,1,2]
        }
      ]
    }
  };

});
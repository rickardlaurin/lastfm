angular.module('lastfm').directive('onEnter', function () {
  'use strict';

  return {
    restrict: 'A',
    link: function (scope, element, attrs, fn) {
      element.bind('keydown', function (e) {
        if (e.keyCode === 13) {
          scope.changeArtist();
        }
      });
    }
  };
});
angular.module('lastfm').service('api', function ($http, $rootScope) {
  'use strict';

  this.get = function (url) {
    var promise = $http.get(url).then(function (res) {
      return res.data;
    }, function (err) {
      $rootScope.serverError = true;
    });

    return promise;
  };
});
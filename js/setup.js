angular.module('lastfm', ['ui.router', 'ngResource','chartjs-directive']);

angular.module('lastfm').config(function ($stateProvider, $urlRouterProvider) {
  'use strict';

  $stateProvider.state('chart', {
    url: '/',
    templateUrl: 'partial/chart/chart.html'
  });
	/* Add New Routes Above */
  
  // For any unmatched url, redirect to /
  $urlRouterProvider.otherwise("/");

});

angular.module('lastfm').run(function ($rootScope) {
  'use strict';

  $rootScope.safeApply = function (fn) {
    var phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

});